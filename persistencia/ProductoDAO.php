<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
       
    public function ProductoDAO($idProducto = "", $nombre = "", $cantidad = "", $precio = ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
    }
       
    public function insertar(){
        return "insert into Producto (nombre, cantidad, precio)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio . "')";
    }
    
    public function consultarTodos(){
        return "select idProducto, nombre, cantidad, precio
                from Producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, cantidad, precio
                from Producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idProducto)
                from Producto";
    }
    
}

?>