<?php 
session_start();
require_once "logica/Administrador.php";
require_once "logica/Producto.php";
require_once "logica/Cliente.php";
$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);    
}else{
    $_SESSION["id"]="";
    $_SESSION["rol"]="";
}
if(isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])){
    $_SESSION["id"]="";
}
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>

</head>
<body>
<?php
	$paginasSinSesion = array(
        "presentacion/autenticar.php",
	    "presentacion/cliente/registrarCliente.php",
	    "presentacion/cliente/activarCliente.php"
	);	
	if(in_array($pid, $paginasSinSesion)){
	    include $pid;
	}else if($_SESSION["id"]!="") {
	    if($_SESSION["rol"] == "Administrador"){
	        include "presentacion/menuAdministrador.php";
	    }else if($_SESSION["rol"] == "Cliente"){
	        include "presentacion/menuCliente.php";
	    }	    
	    include $pid;
	}else{
	    include "presentacion/encabezado.php";
	    include "presentacion/inicio.php";
	}
?>

</body>
</html>